<?php

//Load Twig library
require_once 'lib/Twig/Autoloader.php';

//Register Twig
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem( 'templates' );
$twig   = new Twig_Environment( $loader );

echo $twig->render( 'index.twig', array(
	'name'  => 'Twig tutorial',
	'type'  => 'Pagebox',
	'owner' => 'Tomasz Pach',

	'crew' => array(
		array(
			'name'       => 'Tomasz Pach',
			'role'       => 'Dev',
			'status'     => 'good',
			'reg-number' => '01',
			'notes'      => 'note first',
			'badges'     => array( 'gold', 'silver' )
		),
		array(
			'name'       => 'Szymon Katra',
			'role'       => 'Lepszy dev',
			'status'     => 'better',
			'reg-number' => '02',
			'notes'      => 'note second',
			'badges'     => array( 'gold', 'silver', 'bronze' )
		),
		array(
			'name'       => 'Piotr Grzesiak',
			'role'       => 'Jeszcze lepszy dev',
			'status'     => 'best',
			'reg-number' => '03',
			'notes'      => 'note third',
			'badges'     => array( 'bronze' )
		),
	)
) );

?>